# Docker Android Gradle 

Forked from https://github.com/Cangol/android-gradle-docker

They recently bumped the gradle version which broke it for us

Build and push to docker with:

```
sudo docker build -t tickett/android.gradle:latest .
sudo docker push tickett/android.gradle:latest
```

Usage example:

```yaml
stages:
  - build
  - test
  
build:
  stage: build
  image: tickett/android.gradle
  tags:
  - docker
  script:
  - base64 -d $KEYSTORE > /.keystore
  - gradle assembleRelease
  - cp app/build/outputs/apk/release/app-release.apk ./
  artifacts:
    paths:
    - app-release.apk
  
test:
  stage: test
  image: tickett/android.gradle
  tags:
  - docker
  script:
  - gradle test codeCoverage
  - cat app/build/reports/tests/testDebugUnitTest/index.html
  artifacts:
    reports:
      junit: app/build/test-results/testDebugUnitTest/TEST-*.xml
  coverage: '/Total.*?([0-9]{1,3})%/'
```
